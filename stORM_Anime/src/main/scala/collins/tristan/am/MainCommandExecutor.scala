package collins.tristan.am

import collins.tristan.am.model.tables.{UserSeriesTable, VideoSeriesTable, VideoTable}
import collins.tristan.am.model.{UserSeries, Video, VideoSeries, WatchStatus}
import collins.tristan.stORM.dao.ModelDao._
import collins.tristan.stORM.dao.{Identifiable, MixedDaoFactory}
import collins.tristan.util.{CommandExecutor, MainCommand}

import scala.io.StdIn
import scala.util.Try

/**
 * Created by Tristan on 22-Apr-15.
 */
final class MainCommandExecutor(implicit db: MixedDaoFactory) extends CommandExecutor[MainCommand] {
    private val videoDao = db.get[Video].get
    private val userSeriesDao = db.get[UserSeries].get
    private val seriesDao = db.get[VideoSeries].get

    register { case MainCommand.Exit =>
        "Exiting..."
    }

    register { case h: MainCommand.Help =>
        s"${h.cmdName} => ${h.helpText}"
    }

    register { case MainCommand.Query(query) =>
        val strippedQuery = query.stripPrefix("\"").stripSuffix("\"")

        val found = {
            if (query == "*") db.get[Video].get.getAll
            else db.get[Video].get find (VideoTable.mainTitle LIKE strippedQuery as_lowercase)
        }

        formatResults(db, found)
    }

    register { case MainCommand.Watched(seriesName, episodeNum) =>
        watchedEpisode(seriesName, Some(episodeNum))
    }

    register { case MainCommand.WatchedNext(seriesName) =>
        watchedEpisode(seriesName)
    }

    private def selectSeries(selectable: List[VideoSeries]) = {
        val selected = promptSelection("Choose matching series: ", selectable.map(_.title))

        if (selected.isDefined) Some(selected.get -> selectable(selected.get))
        else None
    }

    private def promptSelection(prompt: String, selectable: List[String]): Option[Int] = {
        println(selectable.zipWithIndex.map({case (s, i) => s"[$i] = $s"}).mkString("\n"))
        val response = StdIn.readLine(prompt)
        val res = Try({
            val index = response.toInt
            selectable(index)
            index
        })

        if (res.isSuccess) Some(res.get)
        else None
    }

    private def watchedEpisode(seriesName: String, episodeNum: Option[Int] = None): List[String] = {
        val foundSeries = findSeries(seriesName)

        if (foundSeries.isEmpty) {
            "Unable to find series"
        } else {
            val seriesWatched = {
                if (foundSeries.length == 1) Some(foundSeries.head)
                else {
                    val selected = selectSeries(foundSeries.map(_._2))

                    if (selected.isDefined) Some(foundSeries(selected.get._1))
                    else None
                }
            }

            if (seriesWatched.isDefined) {
                val (video, series, userSeries) = seriesWatched.get

                val ep = {
                    if (episodeNum.isDefined) episodeNum.get
                    else if (userSeries.isEmpty) 1
                    else userSeries.get.episodesWatched + 1
                }

                if (userSeries.isDefined) {
                    if (userSeries.get.episodesWatched < ep) {
                        watchEpisode(series, ep, userSeries)
                        s"Total of $ep episodes watched"
                    } else s"Already watched episode $ep"
                } else {
                    watchEpisode(series, ep, userSeries)
                    List("Added video to my list", s"Watched $ep episodes")
                }
            } else "Invalid series selection"
        }
    }

    private def watchEpisode(series: VideoSeries with Identifiable[Any],
                             episode: Int = 1,
                             userSeries: Option[UserSeries with Identifiable[Any]]): Unit = {
        if (userSeries.isEmpty) {
            userSeriesDao += UserSeries(series, episodesWatched = episode, watchStatus = WatchStatus.Watching)
        } else {
            val modified = userSeries.get.watchEpisodesUntil(episode)

            seriesDao(series.id) = modified.series
            userSeriesDao(userSeries.get.id) = modified
        }
    }

    private def findSeries(mainTitle: String) = {
        val videos = db.get[Video].get find(VideoTable.mainTitle LIKE mainTitle as_lowercase)
        videos.map(v => {
            seriesDao.find(VideoSeriesTable.video EQUALS v.srcId).map(series => {
                val userSeries = {
                    val found = userSeriesDao.find(UserSeriesTable.series EQUALS series.srcId)
                    if (found.length == 1) Some(found.head)
                    else None
                }
                (v, series, userSeries)
            })
        }).flatten
    }

    register { case MainCommand.Invalid =>
        "Invalid command"
    }

    private def formatResults(db: MixedDaoFactory, foundVideos: List[Video with Identifiable[_]]): List[String] = {
        //implicit val db = factory

        val columns = List(40, 5, 5, 15)
        def col(column: Int, value: Any): String = {
            val len = value.toString.length
            val maxLen = columns(column)

            if (len >= maxLen) value.toString.substring(0, len)
            else value.toString + " " * (maxLen - len)
        }

        val userSeriesDao = db.get[UserSeries].get

        val found = for (video <- foundVideos;
                         series <- video.series) yield video -> series.asInstanceOf[VideoSeries with Identifiable[_]]

        s"Found ${found.size} series." +: {
            if (found.nonEmpty) {
                found.map({case (video, series) =>
                    val userSeries = userSeriesDao find(UserSeriesTable.series EQUALS series.srcId)
                    val cols = {
                        if (userSeries.nonEmpty) {
                            List(video.name, series.season, video.genres.size,
                                s"W-${userSeries.head.episodesWatched} of ${series.epReleased}")
                        } else List(video.name, series.season, video.genres.size)
                    }

                    cols.zipWithIndex.map(e => col(e._2, e._1)).mkString
                })
            } else Nil
        }
    }
}
