package collins.tristan.am

import java.io.File

import collins.tristan.am.model._
import collins.tristan.am.model.tables._
import collins.tristan.stORM.dao._
import collins.tristan.stORM.observer.Observable
import collins.tristan.stORM.source._
import collins.tristan.stORM.sources._
import collins.tristan.stORM.sources.jdbc.MySQLSourceFactory
import collins.tristan.util.MainCommand
import com.googlecode.lanterna.gui.GUIScreen.Position
import com.googlecode.lanterna.gui._
import com.googlecode.lanterna.gui.component._
import com.googlecode.lanterna.gui.layout.{LinearLayout, VerticalLayout}
import org.joda.time.{LocalDateTime, Period}

import scala.collection.mutable.ListBuffer
import scala.io.StdIn
import scala.reflect.ClassTag

object MySQLDB extends AnimeBaseDB {
    private val USERNAME = "user"
    private val PASSWORD = "pass"

    override protected def sourceFactory(dbName: String)
                                        (implicit tableFor: ClassTag[_] => Option[TableDef[_, _]]) = {
        new MySQLSourceFactory(dbName, USERNAME, PASSWORD,
            tableAliases = Map(
                VideoSeriesTable -> "video_series",
                UserSeriesTable -> "user_series",
                GenreTable -> "genre",
                VideoGenreTable -> "video_genre",
                MovieTable -> "video_movie"/*,
                DownloadStatusTable -> "status_download",
                WatchStatusTable -> "status_watch"*/
            )
        )
    }
}

object MongoDB extends AnimeBaseDB {
    override protected def sourceFactory(dbName: String)
                                        (implicit tableFor: ClassTag[_] => Option[TableDef[_, _]]) = {
        new MongoSourceFactory(dbName)
    }
}

object CSVDB extends AnimeBaseDB {
    override protected def sourceFactory(dbName: String)
                                        (implicit tableFor: ClassTag[_] => Option[TableDef[_, _]]) = {
        new DelimitedSourceFactory(new File(new File("db", dbName), "csv"), ",", "csv")
    }
}

object JsonDB extends AnimeBaseDB {
    override protected def sourceFactory(dbName: String)
                                        (implicit tableFor: (ClassTag[_]) => Option[TableDef[_, _]]) = {
        new JSONSourceFactory("db/anime_manager/json", "json")
    }
}

object main {
    implicit val curDb = CSVDB
    val executor = new MainCommandExecutor()

    private var running = true

    {
        def tag[T: ClassTag]: ClassTag[T] = implicitly[ClassTag[T]]
        val models: List[ClassTag[_]] = List(
            tag[Country],
            /*tag[DownloadStatus],
            tag[WatchStatus],*/
            tag[Video],
            tag[VideoSeries],
            tag[UserSeries],
            tag[Genre],
            tag[VideoGenre],
            tag[Movie]
        )

        //copyDb(MySQLDB, MongoDB, models)
        //copyDb(MySQLDB, CSVDB, models)
        //copyDb(MySQLDB, JsonDB, models)
    }

    def copyDb(src: MixedDaoFactory, dst: MixedDaoFactory, models: List[ClassTag[_]], clearDst: Boolean = false): Unit = {
        models.foreach(tag => {
            val dstDao = dst.get(tag).get.asInstanceOf[ModelDao[Any, Any]]
            val dstAll = dstDao.getAll

            if (clearDst) dstDao.remove(dstAll)

            val srcDao = src.get(tag).get.asInstanceOf[ModelDao[Any, Any]]
            val all = srcDao.getAll.map(m => srcDao.idOf(m).get -> m)

            {
                if (dstAll.isEmpty || clearDst) all
                else all.filter(m => dstDao(m._2.id).isEmpty)
            }.foreach(entry => dstDao(entry._1) = entry._2)

            /*if (clearDst || dstAll.isEmpty) {
                val srcDao = src.get(tag).get.asInstanceOf[ModelDao[Any, Any]]
                val all = srcDao.getAll.map(m => srcDao.idOf(m).get -> m)

                all.foreach(entry => dstDao(entry._1) = entry._2)
            }*/
        })
    }

    private def benchmark[T](block: => T): (Int, T) = {
        val start = LocalDateTime.now
        val res: T = block
        val end = LocalDateTime.now

        (new Period(start, end).getMillis, res)
    }

    private def benchmark(block: => Unit): Int = {
        val start = LocalDateTime.now
        block
        val end = LocalDateTime.now

        new Period(start, end).getMillis
    }

    def main(args: Array[String]): Unit = {
        /*val screen = TerminalFacade.createScreen()
        val gui = TerminalFacade.createGUIScreen(screen)
        screen.startScreen()

        val window = new MyWindow(gui, "Anime DB")
        window.OnTextSubmitted += handleCommand

        val allMovies = curDb.movies.getAll
        window.writeLines(s"Found ${allMovies.length} ${if (allMovies.length == 1) "movie" else "movies"}" +: allMovies.map(m => {
            s"${m.name} ${m.runtime}mins ${m.country} ${m.genres.size} Genres"
        }))

        window.show()*/
        /*{
            val (time, videos) = benchmark {
                curDb.videos find (VideoTable.mainTitle LIKE "a" as_lowercase)
            }
            println(s"$time ms to find ${videos.length} videos")

            val (readTime, series) = benchmark {
                (for (video <- videos) yield video.series).flatten
            }
            println(s"$readTime ms to read ${series.length} series")

            println(series.take(10).map(s => s.video.mainTitle))

            running = false
        }*/

        while (running) {
            val cmd: MainCommand = StdIn.readLine("-->")
            println(executor.execute(cmd).mkString("\n"))

            if (cmd == MainCommand.Exit) running = false
        }
        //screen.stopScreen()

        //while (running) {}
    }

    def handleCommand(stop: () => Unit, args: (MyWindow, String)): Unit = {
        val cmd: MainCommand = args._2

        args._1.writeLines(executor.execute(cmd))

        if (cmd == MainCommand.Exit) {
            running = false
            args._1.close()
        }
    }
}

class MyWindow(gui: GUIScreen, title: String) {
    val OnTextSubmitted = new Observable[(MyWindow, String)]() {}

    private val _window = new Window(title)
    private val buffer = ListBuffer[Char]()

    private lazy val txtArea = new TextArea
    private lazy val inputPreview = new TextBox
    private lazy val submitButton = new Button("Execute", new Action {
        override def doAction(): Unit = {
            OnTextSubmitted((MyWindow.this, inputPreview.getText))
        }
    })

    initWindow()

    private def initWindow(): Unit = {
        def onTextSubmitted(stop: () => Unit, args: (MyWindow, String)): Unit = {
            inputPreview.setText("")
            writeLine(args._2)
        }

        val panel = new Panel()
        panel.setLayoutManager(new VerticalLayout)

        panel.addComponent(txtArea, LinearLayout.MAXIMIZES_HORIZONTALLY, LinearLayout.MAXIMIZES_VERTICALLY)
        panel.addComponent(inputPreview, LinearLayout.MAXIMIZES_HORIZONTALLY)
        panel.addComponent(submitButton, LinearLayout.MAXIMIZES_HORIZONTALLY)

        _window.addComponent(panel)

        OnTextSubmitted += onTextSubmitted
    }

    def show(): Unit = {
        gui.showWindow(_window, Position.FULL_SCREEN)

        writeLines("Anime Database v0.1", "Created by Tristan Collins", "Powered by stORM", "", "", "Welcome!!!")
    }

    def close(): Unit = {
        _window.close()
    }

    def writeLine(line: String): Unit = {
        txtArea.appendLine(line)
    }

    def writeLines(lines: String*): Unit = writeLines(lines.toArray)

    def writeLines(lines: Iterable[String]): Unit = {
        lines.foreach(writeLine)
    }
}
