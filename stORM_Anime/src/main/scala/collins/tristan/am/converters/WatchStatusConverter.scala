package collins.tristan.am.converters

import collins.tristan.am.model.WatchStatus
import collins.tristan.stORM.converters.OrdEnumConverter

/**
 * Created by Tristan on 15-Apr-15.
 */
class WatchStatusConverter extends OrdEnumConverter(WatchStatus)
