package collins.tristan.am.converters

import collins.tristan.am.model.DownloadStatus
import collins.tristan.stORM.converters.OrdEnumConverter

/**
 * Created by Tristan on 15-Apr-15.
 */
class DownloadStatusConverter extends OrdEnumConverter(DownloadStatus)