package collins.tristan.am.model

import collins.tristan.stORM.converters.OrdEnumConverter
import collins.tristan.stORM.{BaseEnumOrd, OrdEnum}

/**
 * Created by Tristan on 15-Apr-15.
 */
case class UserSeries(series: VideoSeries, episodesDownloaded: Int = 0, episodesWatched: Int = 0, rating: Int = 0,
                         downloadStatus: DownloadStatus = DownloadStatus.NotDownloaded,
                      watchStatus: WatchStatus = WatchStatus.Undecided, userId: Int = 3) {
    def watchEpisode(): UserSeries = watchEpisodesUntil(episodesWatched + 1)

    def watchEpisodesUntil(episode: Int): UserSeries = {
        if (episodesWatched >= episode) this
        else {
            var s = series
            if (s.epReleased < episode) s = s.episodeReleased(episode)

            copy(s, episodesDownloaded, episode, rating, downloadStatus, watchStatus, userId)
        }
    }
}

sealed abstract class DownloadStatus(name: String, ord: Int) extends BaseEnumOrd(ord, name)

object DownloadStatus extends OrdEnum[DownloadStatus] {
    case object Deleted extends DownloadStatus("Deleted", 1)
    case object Complete extends DownloadStatus("Complete", 2)
    case object NotDownloaded extends DownloadStatus("Not Downloaded", 3)
    case object Incomplete extends DownloadStatus("Incomplete", 4)

    override def values: Set[DownloadStatus] = Set(Deleted, Complete, NotDownloaded, Incomplete)

    implicit val converter = new OrdEnumConverter(DownloadStatus)
}

sealed abstract class WatchStatus(name: String, ord: Int) extends BaseEnumOrd(ord, name)

object WatchStatus extends OrdEnum[WatchStatus] {
    case object Undecided extends WatchStatus("Undecided", 1)
    case object Watched extends WatchStatus("Watched", 2)
    case object NotWatching extends WatchStatus("Not Watching", 3)
    case object Watching extends WatchStatus("Watching", 4)
    case object ToWatch extends WatchStatus("To Watch", 5)
    case object OnHold extends WatchStatus("On Hold", 6)

    override def values: Set[WatchStatus] = Set(Undecided, Watched, NotWatching, Watching, ToWatch, OnHold)

    implicit val converter = new OrdEnumConverter(WatchStatus)
}