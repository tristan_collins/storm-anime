package collins.tristan.am.model.old

import collins.tristan.stORM.source.TableDef
import collins.tristan.stORM.converters.default._

class Series(val season: Int, val name: String) {
    protected[this] def this() = this(1, "")
}

object Series {
    def apply(season: Int, name: String): Series = new Series(season, name)
}

object SeriesTable extends TableDef[Series, Int] {
    def season = column[Int]("season")
    def name = column[String]("name")
}