package collins.tristan.am.model.tables

import collins.tristan.am.model.{Video, VideoSeries}
import collins.tristan.stORM.converters.default._
import collins.tristan.stORM.dao.Identifiable
import collins.tristan.stORM.source.{SourceValue, TableDef}

object VideoSeriesTable extends TableDef[VideoSeries, Int] {
    val video = foreignKey("video", VideoTable)
    val myAnimeListId = column[Int]("malId")
    val season = column[Int]("season")
    val seriesType = column[String]("seriesType")
    val epReleased = column[Int]("epReleased")
    val epToRelease = column[Int]("epToRelease")

    video as "id_video"
    myAnimeListId as "id_myanimelist"
    season as "series_num"
    seriesType as "series_type"
    epReleased as "episodes_released"
    epToRelease as "episodes_to_release"

    def query(video: Video with Identifiable[_]): Map[String, SourceValue] = {
        Map(VideoSeriesTable.video.name -> video.srcId)
    }
}