package collins.tristan.am.model

class VideoSeries (val video: Video, val malId: Int, val season: Int, val seriesType: String,
                   val epReleased: Int, val epToRelease: Int) {
    def episodeReleased(episode: Int) = {
        if (episode < epReleased) this
        else new VideoSeries(video, malId, season, seriesType, episode,
            if (epToRelease < episode) episode else epToRelease)
    }

    def title: String = {
        val main = video.mainTitle
        val sub = video.subTitle

        if (sub.isEmpty) main
        else s"$main: $sub"
    }
}
