package collins.tristan.am.model

/**
 * Created by Tristan on 19-Apr-15.
 */
case class Genre(name: String)
