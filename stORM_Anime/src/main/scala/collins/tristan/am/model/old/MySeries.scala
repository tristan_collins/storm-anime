package collins.tristan.am.model.old

import collins.tristan.stORM.dao.ModelDao
import collins.tristan.stORM.source.{SourceValue, TableDef}
import collins.tristan.stORM.converters.default._

class MySeries(val series: Series, val interested: Boolean = true, val episodesWatched: Set[WatchedEpisode] = Set())

class WatchedEpisode(val series: MySeries, val episode: Int)

object MySeries {
    def apply(series: Series, interested: Boolean = true): MySeries = new MySeries(series, interested)

    object Query {
        def apply[Id](series: Series)(implicit seriesDao: ModelDao[Id, Series], c: Id => SourceValue): Map[String, SourceValue] = {
            Map(MySeriesTable.series.name -> seriesDao.idOf(series).get)
        }
    }
}

object WatchedEpisode {
    def apply(series: MySeries, episode: Int): WatchedEpisode = new WatchedEpisode(series, episode)
}

object MySeriesTable extends TableDef[MySeries, Int] {
    def series = foreignKey("series", SeriesTable)
    def interested = column[Boolean]("interested")
    def episodesWatched = oneToMany("episodesWatched", WatchedEpisodeTable)(_.series)
}

object WatchedEpisodeTable extends TableDef[WatchedEpisode, Int] {
    def series = foreignKey("series", MySeriesTable)
    def episode = column[Int]("episode")
}