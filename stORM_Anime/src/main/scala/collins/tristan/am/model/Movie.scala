package collins.tristan.am.model

/**
 * Created by Tristan on 18-Apr-15.
 */
class Movie(mainTitle: String, subTitle: String, videoType: String, country: String,
            val runtime: Int = 0, val lang: String = "en", val releaseStatus: Option[String] = None,
            val plot: Option[String] = None, genres: Set[Genre] = Set())
    extends Video(mainTitle, subTitle, videoType, country, genres = genres) {
    private val hiddenCode: Option[Int] = Some(54)

    def addGenres(genres: Genre*): Movie =
        new Movie(mainTitle, subTitle, videoType, country, runtime, lang, releaseStatus, plot, this.genres ++ genres)
}