package collins.tristan.am.model

class Video(val mainTitle: String, val subTitle: String, val videoType: String, val country: String,
            val genres: Set[Genre] = Set()) {
    val series: Set[VideoSeries] = Set()
    val movies: Set[Movie] = Set()

    final def name: String = {
        val tail = if (subTitle.isEmpty) "" else s": $subTitle"
        s"$mainTitle$tail"
    }
}
