package collins.tristan.am.model.old

import java.io.File

import collins.tristan.stORM.dao.id.PersistentIntIdGenerator
import collins.tristan.stORM.dao.{IdGenerator, SimpleDaoFactory}
import collins.tristan.stORM.source.DataSourceFactory
import collins.tristan.stORM.sources.JSONSourceFactory

import scala.reflect.ClassTag

/**
 * Created by Tristan on 15-Apr-15.
 */
object AnimeDB extends SimpleDaoFactory[Int] {
    private val TABLE_PATH = new File("db", "tables")

    register(SeriesTable)
    register(MySeriesTable)
    register(WatchedEpisodeTable)

    override protected def newIdGen[Model: ClassTag]: IdGenerator[Int] = {
        if (!TABLE_PATH.exists()) TABLE_PATH.mkdirs()

        PersistentIntIdGenerator[Model](TABLE_PATH)
    }

    override protected val sourceFactory: DataSourceFactory[String] = JSONSourceFactory("db/tables")
}
