package collins.tristan.am.model.tables

import collins.tristan.am.model.Movie
import collins.tristan.stORM.converters.default._
import collins.tristan.stORM.source.TableDef

/**
 * Created by Tristan on 18-Apr-15.
 */
object MovieTable extends TableDef[Movie, Int] {
    setParent(VideoTable)

    val runtime = column[Int]("runtime")
    val plot = optional[String]("plot")
    val releaseStatus = optional[String]("releaseStatus")
    val lang = column[String]("lang")

    runtime as "runtime_mins"
    releaseStatus as "status_release"

    override val id: String = "id_video"
}
