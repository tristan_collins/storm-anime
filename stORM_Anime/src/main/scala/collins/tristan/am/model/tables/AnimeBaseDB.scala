package collins.tristan.am.model.tables

import java.io.File

import collins.tristan.am.model._
import collins.tristan.stORM.dao.id.PersistentIntIdGenerator
import collins.tristan.stORM.dao.{IdGenerator, MixedDaoFactory}
import collins.tristan.stORM.source.{TableDef, DataSourceFactory}
import collins.tristan.stORM.sources.MongoSourceFactory

import scala.reflect.ClassTag

/**
 * Created by Tristan on 15-Apr-15.
 */
abstract class AnimeBaseDB(dbName: String = "anime_manager", relIdPath: String = "db") extends MixedDaoFactory {
    private val DB_NAME = dbName
    private lazy val ID_PATH = new File(relIdPath, DB_NAME)

    register[Country, Int](CountryTable)
    register[Video, Int](VideoTable)
    register[VideoSeries, Int](VideoSeriesTable)
    register[UserSeries, Int](UserSeriesTable)
    register[Movie, Int](MovieTable)
    register[Genre, Int](GenreTable)
    register[VideoGenre, Int](VideoGenreTable)
    /*register[DownloadStatus, Int](DownloadStatusTable)
    register[WatchStatus, Int](WatchStatusTable)*/

    override protected def newIdGen[Id: ClassTag, Model: ClassTag]: IdGenerator[Id] = {
        if (!ID_PATH.exists()) ID_PATH.mkdirs()

        PersistentIntIdGenerator[Model](ID_PATH).asInstanceOf[IdGenerator[Id]]
    }

    final override protected def sourceFactory: DataSourceFactory[String] = sourceFactory(DB_NAME)

    protected def sourceFactory(dbName: String)
                               (implicit tableFor: ClassTag[_] => Option[TableDef[_, _]]): DataSourceFactory[String]

    lazy val countries = get[Int, Country].get
    lazy val videos = get[Int, Video].get
    lazy val videoSeries = get[Int, VideoSeries].get
    lazy val userSeries = get[Int, UserSeries].get
    lazy val movies = get[Int, Movie].get
}