package collins.tristan.am.model.tables

import collins.tristan.am.model.Country
import collins.tristan.stORM.converters.default._
import collins.tristan.stORM.source.TableDef

object CountryTable extends TableDef[Country, Int] {
    val nameOfCountry = column[String]("name")

    nameOfCountry as "name"
}