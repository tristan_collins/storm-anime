package collins.tristan.am.model.tables

import collins.tristan.am.model.{Genre, Video}
import collins.tristan.stORM.converters.default._
import collins.tristan.stORM.source.{ForeignKey, JoinTable, TableDef}

object VideoTable extends TableDef[Video, Int] {
    val mainTitle = column[String]("mainTitle")
    val subTitle = column[String]("subTitle")
    val videoType = column[String]("videoType")
    val country = column[String]("country")
    val series = oneToMany("series", VideoSeriesTable)(_.video)
    val genres = join("genres", VideoGenreTable)

    mainTitle as "title_main"
    subTitle as "title_sub"
    videoType as "type"
    country as "country_origin"
}

sealed case class VideoGenre(video: Video, genre: Genre)

object VideoGenreTable extends JoinTable[VideoGenre, Int, Video, Int, Genre, Int](VideoTable, GenreTable) {
    override val parentId: ForeignKey[VideoGenre, Video] = foreignKey("video", VideoTable)
    override val childId: ForeignKey[VideoGenre, Genre] = foreignKey("genre", GenreTable)

    parentId as "id_video"
    childId as "id_genre"
}