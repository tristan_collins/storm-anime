package collins.tristan.am.model.tables

import collins.tristan.am.model.DownloadStatus.converter
import collins.tristan.am.model.WatchStatus.converter
import collins.tristan.am.model.{DownloadStatus, UserSeries, VideoSeries, WatchStatus}
import collins.tristan.stORM.converters.default._
import collins.tristan.stORM.dao.MixedDaoFactory
import collins.tristan.stORM.source.{SourceValue, TableDef}

/**
 * Created by Tristan on 15-Apr-15.
 */
object UserSeriesTable extends TableDef[UserSeries, Int] {
    val series = foreignKey("series", VideoSeriesTable)
    val episodesDownloaded = column[Int]("episodesDownloaded")
    val episodesWatched = column[Int]("episodesWatched")
    val rating = column[Int]("rating")
    val userId = column[Int]("userId")
    val downloadStatus = column[DownloadStatus]("downloadStatus")
    val watchStatus = column[WatchStatus]("watchStatus")

    series as "id_series"
    episodesDownloaded as "episodes_downloaded"
    episodesWatched as "episodes_watched"
    downloadStatus as "status_download_user"
    watchStatus as "status_watch_user"
    userId as "id_user"

    def query(series: VideoSeries)(implicit db: MixedDaoFactory): Map[String, SourceValue] = {
        Map(UserSeriesTable.series.name -> db.get[VideoSeries].get.srcId(series).get)
    }
}