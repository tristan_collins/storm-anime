package collins.tristan.am.model.tables

import collins.tristan.am.model.Genre
import collins.tristan.stORM.converters.default._
import collins.tristan.stORM.source.TableDef

object GenreTable extends TableDef[Genre, Int] {
    val name = column[String]("name")
}