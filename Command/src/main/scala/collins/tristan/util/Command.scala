package collins.tristan.util

import scala.collection.immutable.Queue

trait Command
trait CommandParam

object Command {
    def parseParams(_str: String): Queue[CommandParam] = {
        val ESC_SPACE = "$_SPACE"

        def escape(s: String): String = {
            val qPattern = "(\"[^\"]*\") ".r
            var escString = s

            if (escString.trim.endsWith("\"")) {
                escString += " "
            }

            qPattern
                .findAllMatchIn(escString)
                .foreach(m => {
                val mStr = m.toString().trim
                val escaped = mStr.replace(" ", ESC_SPACE)
                escString = escString.replace(mStr, escaped)
            })

            if (escString.endsWith(ESC_SPACE)) escString.take(escString.length - ESC_SPACE.length)
            else escString
        }

        def descape(s: String): String = s.replace(ESC_SPACE, " ").trim
        def clean(s: String): String = descape(s).stripSuffix("\"").stripPrefix("\"")

        val escString = escape(_str)
        val split = escString.split(" ")
        var params = Queue[CommandParam]()

        for (s <- split; if !s.isEmpty) {
            if (s.contains("=")) {
                val np = s.split("=")
                params = params :+ NamedParam(clean(np(0)), clean(np(1)))
            } else params = params :+ ValueParam(clean(s))
        }

        params
    }
}

final case class ValueParam(value: String) extends CommandParam
final case class NamedParam(key: String, value: String) extends CommandParam

trait CommandEnvironment[C <: Command] {
    def parseCommand(cmd: ValueParam, params: Queue[CommandParam]): C

    final protected def getNamedParam(key: String, params: Queue[CommandParam]): Option[NamedParam] = {
        val found = params.filter({
            case _: NamedParam => true
            case _ => false
        }).find(_.asInstanceOf[NamedParam].key == key)

        if (found.isDefined) Some(found.get.asInstanceOf[NamedParam])
        else None
    }
}