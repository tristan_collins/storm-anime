package collins.tristan.util

import scala.collection.immutable.Queue

sealed trait MainCommand extends Command

object MainCommand extends CommandEnvironment[MainCommand] {
    object Invalid extends MainCommand
    object Exit extends MainCommand
    case class Query(query: String = "*") extends MainCommand
    case class Watched(seriesName: String, episodeNum: Int) extends MainCommand
    case class WatchedNext(seriesName: String) extends MainCommand
    case class Help(cmdName: String) extends MainCommand {
        val helpText = {
            if (cmdHelpText.contains(cmdName)) cmdHelpText(cmdName)
            else "No help available"
        }
    }

    private val cmdHelpText = Map(
        "list" -> "list [title]",
        "watched" -> "watched series=[string] episode=[int]",
        "exit" -> "exit"
    )

    implicit def str2Command(str: String): MainCommand = {
        val params = Command.parseParams(str)

        if (params.length == 0) Invalid
        else {
            val cmd = params.head.asInstanceOf[ValueParam]
            parseCommand(cmd, params.drop(1))
        }
    }

    def parseCommand(cmd: ValueParam, params: Queue[CommandParam]): MainCommand = {
        if (params.length == 1 && params.head == ValueParam("--help")) Help(cmd.value)
        else {
            cmd.value match {
                case "list" => {
                    val qParam = getNamedParam("q", params)
                    if (qParam.isDefined) Query(qParam.get.value)
                    else if (params.isEmpty) Query()
                    else if (params.length == 1) params.head match {
                        case ValueParam(x) => Query(x)
                        case _ => Invalid
                    }
                    else Invalid
                }
                case "watched" if getNamedParam("series", params).isDefined => {
                    val seriesName = getNamedParam("series", params).get.value
                    val strippedName = seriesName.stripPrefix("\"").stripSuffix("\"")

                    if (strippedName.isEmpty) Invalid
                    else if (params.contains("episode")) {
                        val episodeNum = getNamedParam("episode", params)

                        if (episodeNum.isEmpty) Invalid
                        else Watched(strippedName, episodeNum.get.value.toInt)
                    } else {
                        WatchedNext(strippedName)
                    }
                }
                case "watched" => {
                    val seriesName = params.head.asInstanceOf[ValueParam].value
                    val strippedName = seriesName.stripPrefix("\"").stripSuffix("\"")

                    if (strippedName.isEmpty) Invalid
                    else if (params.length == 2) {
                        val episodeNum = params(1).asInstanceOf[ValueParam].value

                        Watched(strippedName, episodeNum.toInt)
                    } else WatchedNext(strippedName)
                }
                case "exit" | "q" | "esc" => Exit
                case _ => Invalid
            }
        }
    }
}