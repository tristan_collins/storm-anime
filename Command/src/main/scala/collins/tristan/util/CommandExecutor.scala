package collins.tristan.util

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
 * Created by Tristan on 22-Apr-15.
 */
trait CommandExecutor[C <: Command] {
    private val commands = ListBuffer[PartialFunction[C, List[String]]]()

    final protected def register(cmd: PartialFunction[C, List[String]]): Unit = commands += cmd
    final protected implicit def strToList(str: String): List[String] = List(str)

    final def execute(cmd: C): List[String] = {
        val cmdFunc = commands.find(_.isDefinedAt(cmd))

        if (cmdFunc.isDefined) cmdFunc.get(cmd)
        else "Unable to execute command"
    }
}
